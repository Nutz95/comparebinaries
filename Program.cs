﻿using CompareBinaries.Diff;
using System;
using System.IO;
using System.Reflection;

namespace CompareBinaries
{
    class Program
    {
        private const string _bin1Argument = "-binary1";
        private const string _bin2Argument = "-binary2";
        private const string _helpArgument1 = "-?";
        private const string _helpArgument2 = "-help";
        private const string _helpArgument3 = @"\?";
        private const string _helpArgument4 = @"\help";
        private const string _helpArgument5 = "--?";
        private const string _helpArgument6 = "--help";
        private const string _debugArgument = "-verbose";

        static void Main(string[] args)
        {
            var binaryPath1 = string.Empty;
            var binaryPath2 = string.Empty;

            if(!HandleArgs(args, out binaryPath1, out binaryPath2))
            {
                Environment.Exit(-1);
            }

            var FileComparison = new FileComparison(binaryPath1, binaryPath2);

            Environment.Exit(FileComparison.AreBothBinariesIdentical() ? 0 : -1);
        }

        private static void CheckForSingleArgs(string argument)
        {
            switch (argument.ToLower())
            {
                case _helpArgument1:
                case _helpArgument2:
                case _helpArgument3:
                case _helpArgument4:
                case _helpArgument5:
                case _helpArgument6:
                    DisplayHelp();
                    break;
                case _debugArgument:
                    Logger.Level = LogLevel.Debug;
                    break;
                default:
                    break;
            }
        }

        private static bool HandleArgs(string[] args, out string binaryToAnalyse1, out string binaryToAnalyse2)
        {
            binaryToAnalyse1 = string.Empty;
            binaryToAnalyse2 = string.Empty;

            // Help page
            if(args.Length == 1)
            {
                CheckForSingleArgs(args[0]);
                Environment.Exit(-2);
            }

            for (int i = 0; i < args.Length; i++)
            {
                if( i > args.Length -2)
                {

                    CheckForSingleArgs(args[i]);
                    break;
                }
                
                switch(args[i].ToLower())
                {
                    case _bin1Argument:
                        if(!File.Exists(args[i + 1]))
                        {
                            Logger.Log($"Invalid argument for {_bin1Argument}");
                            return false;
                        }
                        binaryToAnalyse1 = args[i + 1];
                        Logger.Log($"First file to compare (F1): {binaryToAnalyse1}");
                        break;
                    case _bin2Argument:
                        if (!File.Exists(args[i + 1]))
                        {
                            Logger.Log($"Invalid argument for {_bin2Argument}");
                            return false;
                        }
                        binaryToAnalyse2 = args[i + 1];
                        Logger.Log($"Second file to compare (F2): {binaryToAnalyse2}");
                        break;
                    default:
                        CheckForSingleArgs(args[i]);
                        break;
                }
            }
            if (!string.IsNullOrEmpty(binaryToAnalyse1) && !string.IsNullOrEmpty(binaryToAnalyse2))
            {
                return true;
            }
            return false;
        }

        private static void DisplayHelp()
        {
            Logger.Log("------------------------- HELP PAGE -------------------------");
            Logger.Log($"{_helpArgument1} {_helpArgument2} {_helpArgument3} {_helpArgument4} {_helpArgument5} {_helpArgument6} => display this help page");
            Logger.Log($"(Optional) {_debugArgument} => display verbose output with more details");
            Logger.Log($"(mandatory) {_bin1Argument} [PathToBInaryFile1] full path to binary file 1 to perform comparison");
            Logger.Log($"(mandatory) {_bin2Argument} [PathToBInaryFile2] full path to binary file 2 to perform comparison");
            Logger.Log("");
            Logger.Log($"Exemple: {Assembly.GetExecutingAssembly().GetName().Name} {_bin1Argument} d:\\binary1.dll {_bin2Argument} d:\\binary2.dll");
            Logger.Log("");
            Logger.Log("return values:\r\n0 = Comparison OK\r\n-1 = Comparison Error\r\n-2 = Help page displayed");
        }
    }
}

