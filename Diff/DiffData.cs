﻿namespace CompareBinaries.Diff
{
    public class DiffData
    {
        public string LineOfFile1 { get; private set; }
        public string LineOfFile2 { get; private set; }
        public long LineIndex { get; private set; }

        public DiffData(string lineOfFile1,
                        string lineOfFile2,
                        long lineIndex)
        {
            LineOfFile1 = lineOfFile1;
            LineOfFile2 = lineOfFile2;
            LineIndex = lineIndex;
        }
    }
}
