﻿using CompareBinaries.Disassemble;
using CompareBinaries.PeHeaders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace CompareBinaries.Diff
{
    public class FileComparison
    {
        private const string _comparisonErrorString = "\t/!\\ are different.";
        private const string _comparisonOKString = "OK.";

        private static List<string> ListOfNonCriticalDifferencesPatterns = new List<string>()
        {
            @"^.*\d{1,3}:\d{1,3}:\d{1,3}(?:\d{1,6})?$",
            @"^.*custom.*instance void \[System.Runtime.*System.Reflection.AssemblyFileVersionAttribute.*=.*\(.*\).*// \.*\d{1,3}\.\d{1,3}\.\d{1,3}.\d{1,3}.*",
            @"^.*custom.*instance void \[System.Runtime.*System.Reflection.AssemblyInformationalVersionAttribute.*=.*\(.*\).*// \.*\d{1,3}\.\d{1,3}\.\d{1,3}.*",
        };

        private string _InputFile1 { get; set; }
        private string _InputFile2 { get; set; }

        public FileComparison(string inputFile1, string inputFile2)
        {
            _InputFile1 = inputFile1;
            _InputFile2 = inputFile2;
        }

        public bool AreBothBinariesIdentical()
        {
            var peBinary1 = new PeHeaderReader(_InputFile1);
            var peBinary2 = new PeHeaderReader(_InputFile2);

            var arePeSimilar = ComparePeDosHeadersFromBinaries(peBinary1.DosHeader, peBinary2.DosHeader);
            arePeSimilar &= ComparePeImageHeaders(peBinary1, peBinary2);
            arePeSimilar &= CompareFileImages(peBinary1, peBinary2);
            arePeSimilar &= ComparePeOptionalHeaders(peBinary1, peBinary2, _InputFile1, _InputFile2);
            arePeSimilar &= CompareImageFileBinary(_InputFile1, _InputFile2);
            return arePeSimilar;
        }

        // both files are supposed to have the same headers
        private bool CompareImageFileBinary(string filePath1,
                                                   string filePath2)
        {
            Logger.LogDebug("------------Comparing File Binaries of both input files-----------------");
            var disassemblyContent1 = new HashAndDisassemble(filePath1).GetDisassembledFile();
            var disassemblyContent2 = new HashAndDisassemble(filePath2).GetDisassembledFile();

            if (string.IsNullOrEmpty(disassemblyContent1))
            {
                Logger.LogDebug($"Error: Couldn't desassemble file {filePath1}");
                return false;
            }
            if (string.IsNullOrEmpty(disassemblyContent2))
            {
                Logger.LogDebug($"Error: Couldn't desassemble file {filePath2}");
                return false;
            }

            return AreDisassembledFilesDifferent(disassemblyContent1, disassemblyContent2);
        }

        /// <summary>
        /// Compares 2 array of string and output a list of the differences
        /// </summary>
        /// <param name="linesOfFile1"></param>
        /// <param name="linesOfFile2"></param>
        /// <returns></returns>
        private List<DiffData> ExtractDiffFromLines(string[] linesOfFile1, string[] linesOfFile2)
        {
            List<DiffData> listOfDifferences = new List<DiffData>();
            for (long i = 0; i < linesOfFile1.Length; i++)
            {
                if (linesOfFile1[i] != linesOfFile2[i])
                {
                    listOfDifferences.Add(new DiffData(linesOfFile1[i], linesOfFile2[i], i));
                }
            }
            return listOfDifferences;
        }

        /// <summary>
        /// Compares IMAGE_DATA_DIRECTORY headers
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="optionHeader1"></param>
        /// <param name="optionHeader2"></param>
        /// <param name="OsType"></param>
        /// <returns>true if both headers matches. false if both headers have a difference</returns>
        private bool compareOptionHeaders<T>(T optionHeader1, T optionHeader2, string OsType)
        {
            bool outputResult = true;
            const int sizeOfColumn = 30;

            Logger.LogDebug($"------------Comparing {OsType} Optional Headers of both input files-----------------");
            Logger.LogDebug($"{"ParameterName",sizeOfColumn}\t{"F1",sizeOfColumn}\t{"F2",sizeOfColumn}\t{"Status",sizeOfColumn}");

            foreach (var field in typeof(T).GetFields(BindingFlags.Instance |
                                                      BindingFlags.Public))
            {
                var differentString = $"{_comparisonOKString,sizeOfColumn}";
                if (field.FieldType == typeof(IMAGE_DATA_DIRECTORY))
                {
                    foreach (var imageDataDirectoryField in field.FieldType.GetFields(BindingFlags.Instance |
                                                                                      BindingFlags.Public))
                    {
                        var fieldHeader1 = field.GetValue(optionHeader1);
                        var fieldHeader2 = field.GetValue(optionHeader1);
                        var value1 = imageDataDirectoryField.GetValue(fieldHeader1).ToString();
                        var value2 = imageDataDirectoryField.GetValue(fieldHeader2).ToString();
                        if (value1 != value2)
                        {
                            differentString = _comparisonErrorString;
                            outputResult = false;
                        }
                        Logger.LogDebug($"{field.Name,sizeOfColumn}\t{imageDataDirectoryField.Name + ":" + value1,sizeOfColumn}\t{imageDataDirectoryField.Name + ":" + value2,sizeOfColumn}{differentString}");
                    }
                }
                else
                {
                    if (field.GetValue(optionHeader1).ToString() != field.GetValue(optionHeader2).ToString())
                    {
                        differentString = _comparisonErrorString;
                        outputResult = false;
                    }
                    Logger.LogDebug($"{field.Name,sizeOfColumn}\t{field.GetValue(optionHeader1),sizeOfColumn}\t{field.GetValue(optionHeader2),sizeOfColumn}{differentString}");
                }
            }
            Logger.LogDebug("");

            return outputResult;
        }

        private bool ComparePeOptionalHeaders(PeHeaderReader pe1,
                                                    PeHeaderReader pe2,
                                                    string filePath1,
                                                    string filePath2)
        {
            bool outputResult = true;

            if (pe1.Is32BitHeader)
            {
                IMAGE_OPTIONAL_HEADER32 header32Bin1 = pe1.OptionalHeader32;
                IMAGE_OPTIONAL_HEADER32 header32Bin2 = pe2.OptionalHeader32;
                outputResult = compareOptionHeaders(header32Bin1, header32Bin2, "32Bits");
            }
            else // 64 bits header
            {
                IMAGE_OPTIONAL_HEADER64 header64Bin1 = pe1.OptionalHeader64;
                IMAGE_OPTIONAL_HEADER64 header64Bin2 = pe2.OptionalHeader64;
                outputResult = compareOptionHeaders(header64Bin1, header64Bin2, "64Bits");
            }
            return outputResult;
        }

        private bool ComparePeDosHeadersFromBinaries(IMAGE_DOS_HEADER dosHeader1, IMAGE_DOS_HEADER dosHeader2)
        {
            const int sizeOfColumn = 13;
            bool comparisonStatus = true;
            Logger.LogDebug("------------Comparing Dos Header of both input files-----------------");
            Logger.LogDebug($"{"ParameterName",sizeOfColumn}\t{"F1",sizeOfColumn}\t{"F2",sizeOfColumn}\t{"Status",sizeOfColumn}");
            foreach (var field in typeof(IMAGE_DOS_HEADER).GetFields(BindingFlags.Instance |
                                                                      BindingFlags.NonPublic |
                                                                      BindingFlags.Public))
            {
                string DifferentString = $"{_comparisonOKString,sizeOfColumn}";
                if (field.GetValue(dosHeader1).ToString() != field.GetValue(dosHeader2).ToString())
                {
                    DifferentString = _comparisonErrorString;
                    comparisonStatus = false;
                }
                Logger.LogDebug($"{field.Name,sizeOfColumn}\t{field.GetValue(dosHeader1),sizeOfColumn}\t{field.GetValue(dosHeader2),sizeOfColumn}{DifferentString}");
            }
            Logger.LogDebug("");
            return comparisonStatus;
        }

        private bool CompareImageSection(IMAGE_SECTION_HEADER header1, IMAGE_SECTION_HEADER header2)
        {
            const int sizeOfColumn = 25;
            bool comparisonStatus = true;

            foreach (var field in typeof(IMAGE_SECTION_HEADER).GetFields(BindingFlags.Instance |
                                                                      BindingFlags.NonPublic |
                                                                      BindingFlags.Public))
            {
                string DifferentString = $"{_comparisonOKString,sizeOfColumn}";
                if (field.GetValue(header1).ToString() != field.GetValue(header1).ToString())
                {
                    DifferentString = _comparisonErrorString;
                    comparisonStatus = false;
                }
                var value1 = field.GetValue(header1);
                var value2 = field.GetValue(header1);

                if (field.FieldType == typeof(char[]))
                {
                    value1 = new string(header1.Name);
                    value2 = new string(header2.Name);
                    if (!value1.Equals(value2))
                    {
                        DifferentString = _comparisonErrorString;
                        comparisonStatus = false;
                    }
                }

                Logger.LogDebug($"{field.Name,sizeOfColumn}\t{value1,sizeOfColumn}\t{value2,sizeOfColumn}{DifferentString}");
            }
            return comparisonStatus;
        }

        private bool ComparePeImageHeaders(PeHeaderReader pe1, PeHeaderReader pe2)
        {
            const int sizeOfColumn = 25;
            bool comparisonStatus = true;
            Logger.LogDebug("------------Comparing Image Section Headers of both input files-----------------");

            Logger.LogDebug($"{"ParameterName",sizeOfColumn}\t{"F1",sizeOfColumn}\t{"F2",sizeOfColumn}\t{"Status",sizeOfColumn}");

            string DifferentString = $"{_comparisonOKString,sizeOfColumn}";
            if (pe1.ImageSectionHeaders.Length != pe2.ImageSectionHeaders.Length)
            {
                DifferentString = _comparisonErrorString;
                comparisonStatus = false;
            }
            Logger.LogDebug($"{"Image section count",sizeOfColumn}\t{pe1.ImageSectionHeaders.Length,sizeOfColumn}\t{pe1.ImageSectionHeaders.Length,sizeOfColumn}\t{DifferentString,sizeOfColumn}");
            if (comparisonStatus == false)
            {
                return comparisonStatus;
            }

            Logger.LogDebug("\r\n---------------Comparing Image Sections--------------------");
            for (int i = 0; i < pe1.ImageSectionHeaders.Length; i++)
            {
                Logger.LogDebug($"\r\n------------Comparing Image Sections {i + 1} / {pe1.ImageSectionHeaders.Length}-----------------");
                comparisonStatus &= CompareImageSection(pe1.ImageSectionHeaders[i], pe2.ImageSectionHeaders[i]);
            }
            Logger.LogDebug("");
            return comparisonStatus;
        }

        private bool AreDisassembledFilesDifferent(string fileContent1, string fileContent2)
        {
            var LinesFromFile1 = fileContent1.Split("\r\n");
            var LinesFromFile2 = fileContent2.Split("\r\n");

            if (LinesFromFile1.Length != LinesFromFile2.Length)
            {
                Logger.LogDebug($"Error: Length of both input desassembled files is different: {LinesFromFile1.Length} != {LinesFromFile2.Length}");
                Logger.LogDebug($"=> This means that both files BaseCode is different");
                return false;
            }
            if (LinesFromFile1.Length == 0 || LinesFromFile2.Length == 0)
            {
                Logger.LogDebug($"Error: Length of either input desassembled files is 0: {LinesFromFile1.Length} != {LinesFromFile2.Length}");
                return false;
            }

            var listOfDifferences = ExtractDiffFromLines(LinesFromFile1, LinesFromFile2);

            return !CheckIfDifferencesAreCritical(listOfDifferences);
        }

        /// <summary>
        /// Returns false is differences are non critital. Returns true if differences are found to be critical
        /// </summary>
        /// <param name="listOfDifferences"></param>
        /// <returns></returns>
        private bool CheckIfDifferencesAreCritical(List<DiffData> listOfDifferences)
        {
            Logger.LogDebug("------------Comparing Disassembled files content-----------------");
            string diffStatus = "Valid Diff";
            bool areDifferencesCritical = false;
            for (int i = 0; i < listOfDifferences.Count; i++)
            {
                bool isCriticalDiff = true;
                foreach (var pattern in ListOfNonCriticalDifferencesPatterns)
                {
                    if (Regex.IsMatch(listOfDifferences[i].LineOfFile1, pattern))
                    {
                        isCriticalDiff = false;
                        break;
                    }
                }
                if (isCriticalDiff)
                {
                    areDifferencesCritical = isCriticalDiff;
                    diffStatus = "Critical Diff";
                }
                Logger.Log($"Diff {i + 1}/{listOfDifferences.Count} Detected at line {listOfDifferences[i].LineIndex} => {diffStatus}");
                Logger.Log($"{listOfDifferences[i].LineOfFile1}\r\nIs Different from\r\n{listOfDifferences[i].LineOfFile2}\r\n");
            }
            Logger.Log($"***********************************************");
            if (areDifferencesCritical)
            {
                Logger.Log($"*             Comparison Failed               *");
            }
            else
            {
                Logger.Log($"*     Comparison Success (No Major Changes)   *");
            }
            Logger.Log($"***********************************************");
            Logger.Log("");
            return areDifferencesCritical;
        }
        private bool CompareFileImages(PeHeaderReader pe1,
                                            PeHeaderReader pe2)
        {
            const int sizeOfColumn = 25;
            bool comparisonStatus = true;
            Logger.LogDebug("------------Comparing File Header of both input files-----------------");

            var imageFileHeader1 = pe1.FileHeader;
            var imageFileHeader2 = pe1.FileHeader;

            foreach (var field in typeof(IMAGE_FILE_HEADER).GetFields(BindingFlags.Instance |
                                                                      BindingFlags.NonPublic |
                                                                      BindingFlags.Public))
            {
                string DifferentString = $"{_comparisonOKString,sizeOfColumn}";
                if (field.GetValue(imageFileHeader1).ToString() != field.GetValue(imageFileHeader2).ToString())
                {
                    DifferentString = _comparisonErrorString;
                    comparisonStatus = false;
                }
                Logger.LogDebug($"{field.Name,sizeOfColumn}\t{field.GetValue(imageFileHeader1),sizeOfColumn}\t{field.GetValue(imageFileHeader2),sizeOfColumn}{DifferentString}");
            }
            Logger.LogDebug("");
            return comparisonStatus;
        }
    }
}
