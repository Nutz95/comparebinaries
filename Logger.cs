﻿using System;
namespace CompareBinaries
{
    public enum LogLevel
    {
        Debug, 
        Normal
    }

    public static class Logger
    {
        public static LogLevel Level = LogLevel.Normal;
        public static void Log(string message)
        {
            Console.WriteLine(message);
        }
        public static void LogDebug(string message)
        {
            if (Level == LogLevel.Debug)
            {
                Console.WriteLine(message);
            }
        }
    }
}
