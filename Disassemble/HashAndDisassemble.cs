﻿using System;
using System.IO;
using System.Reflection;

namespace CompareBinaries.Disassemble
{
    public class HashAndDisassemble
    {
        public string FileName { get; private set; }

        public HashAndDisassemble(string fileName)
        {
            FileName = fileName;
        }

        /// <summary>
        /// Disassemble binary file and returns the result of the disassembly as a plain text
        /// </summary>
        /// <returns></returns>
        public string GetDisassembledFile()
        {
            if (Path.GetExtension(FileName).Equals(".dll", StringComparison.InvariantCultureIgnoreCase)
                || Path.GetExtension(FileName).Equals(".exe", StringComparison.InvariantCultureIgnoreCase))
            {
                var outputDisassembledResult = GetDisassembledFileContent();
                // checking if the exe might be .net Core where the .exe is not an IL assembly but a native host executable that finds and boots the runtime before loading the actual IL application assembly which has a .dll extension.
                if (string.IsNullOrEmpty(outputDisassembledResult) && Path.GetExtension(FileName).Equals(".exe", StringComparison.InvariantCultureIgnoreCase))
                {
                    var pathToDllEquivalent = Path.GetFullPath(FileName).Replace(".exe", ".dll");
                    if (File.Exists(pathToDllEquivalent))
                    {
                        Logger.LogDebug($"The current file {FileName} appears to be .Net Core executable coming with {pathToDllEquivalent} where the BaseCode is located.\r\nAttempting to Desassemble {pathToDllEquivalent} instead.");
                        outputDisassembledResult = new HashAndDisassemble(pathToDllEquivalent).GetDisassembledFile();
                        if(String.IsNullOrEmpty(outputDisassembledResult))
                        {
                            Logger.LogDebug($"Desassembly of {pathToDllEquivalent} Failed");
                        }
                        else
                        {
                            Logger.LogDebug($"Desassembly of {pathToDllEquivalent} Success");
                        }
                    }
                    Logger.LogDebug("");
                }
                return outputDisassembledResult;
            }
            else return string.Empty;
        }

        /// <summary>
        /// Computes the MD5 Hash of the current Binary file.
        /// </summary>
        /// <returns>Returns the MD5 Hash of the current binary Assembly for .dlls ans .exe. Will return the whole file Md5 for other kinds of files.</returns>
        public string CalculateFileHash()
        {
            if (Path.GetExtension(FileName).Equals(".dll", StringComparison.InvariantCultureIgnoreCase)
                || Path.GetExtension(FileName).Equals(".exe", StringComparison.InvariantCultureIgnoreCase))
            {
                return GetAssemblyFileHash();
            }
            else
            {
                return GetFileHash();
            }
        }

        /// <summary>
        /// Computes the MD5 Hash of the whole current input binary
        /// </summary>
        /// <returns>returns MD5 Hash</returns>
        private string GetFileHash()
        {
            return CalculateHashFromStream(File.OpenRead(FileName));
        }

        /// <summary>
        /// Reads the whole content of the input file path. The main purpose of this function is to retrieve the result of disassembly as plain text.
        /// </summary>
        /// <param name="filename">input file path to read</param>
        /// <returns>The whole binary as string representation</returns>
        private string GetFileContent(string filename)
        {
            if (!File.Exists(filename))
            {
                return string.Empty;
            }
            using (StreamReader sr = new StreamReader(filename))
            {
                return sr.ReadToEnd();
            }
        }

        /// <summary>
        /// Disassemble current binary file and retrives the disassembly result as plain text
        /// </summary>
        /// <returns>disassembly result as plain text</returns>
        private string GetDisassembledFileContent()
        {
            string tempFileName = null;
            try
            {
                //try to open the assembly to check if this is a .NET one
                var assembly = Assembly.LoadFile(FileName);
                tempFileName = Disassembler.GetDisassembledFile(FileName);
                return GetFileContent(tempFileName);
            }
            catch (BadImageFormatException ex)
            {
                Logger.LogDebug($"Unexpected Error when desassembling file {FileName}: {ex.Message}");
                return string.Empty;
            }
            finally
            {
                if (File.Exists(tempFileName))
                {
                    File.Delete(tempFileName);
                }
            }
        }

        /// <summary>
        /// Dissassembles the current binary file and computes the Hash from disassembly output.
        /// </summary>
        /// <returns>MD5 Hash</returns>
        private string GetAssemblyFileHash()
        {
            string tempFileName = null;
            try
            {
                //try to open the assembly to check if this is a .NET one
                var assembly = Assembly.LoadFile(FileName);
                tempFileName = Disassembler.GetDisassembledFile(FileName);
                return CalculateHashFromStream(File.OpenRead(tempFileName));
            }
            catch (BadImageFormatException)
            {
                return GetFileHash();
            }
            finally
            {
                if (File.Exists(tempFileName))
                {
                    File.Delete(tempFileName);
                }
            }
        }

        /// <summary>
        /// Computes Hash from Input Stream
        /// </summary>
        /// <param name="stream"></param>
        /// <returns>MD5 Hash</returns>
        private string CalculateHashFromStream(Stream stream)
        {
            using (var readerSource = new BufferedStream(stream, 1200000))
            {
                using (var md51 = new System.Security.Cryptography.MD5CryptoServiceProvider())
                {
                    md51.ComputeHash(readerSource);
                    return Convert.ToBase64String(md51.Hash);
                }
            }
        }
    }
}
